using PokemonTeamBuilderService.Controllers;
using PokemonTeamBuilderService.Models;
using Moq;
using PokemonTeamBuilderService.Repository;

namespace PokemonTeamBuilderService.Tests
{
    [TestFixture]
    public class PokemonControllerTests
    {
        private PokemonController _pokemonController;
        private PokemonController _pokemonControllerWithMockDeps;
        private Mock<IPokemonDataRepository> _pokemonDataRepositoryMock;

        [SetUp]
        public void InitController()
        {
            PokemonDataRepository dataRepository = new();

            this._pokemonController = new(dataRepository);

            _pokemonDataRepositoryMock = new Mock<IPokemonDataRepository>();
            _pokemonDataRepositoryMock.Setup(x => x.GetPokemonNames()).Returns([]);
            _pokemonDataRepositoryMock.Setup(x => x.SavePokemonNames(It.IsAny<List<PokemonNameDto>>()));
            _pokemonDataRepositoryMock.Setup(x => x.SavePokemonData(It.IsAny<PokemonDto>()));

            this._pokemonControllerWithMockDeps = new(_pokemonDataRepositoryMock.Object);
        }

        [Test]
        public async Task GetAllPokemonNamesAsync_ShouldReturnEnumerable_OnSuccessAsync()
        {
            var result = await _pokemonController.GetAllPokemonNamesAsync();

            Assert.That(result.Count, Is.GreaterThan(0));
        }

        [Test]
        public async Task GetDataForPokemonAsync_ShouldReturnPokemon_OnSuccessAsync()
        {
            var expectedSnorlaxGmax = new PokemonDto()
            {
                Id = 10206,
                Name = "snorlax-gmax",
                SpriteUrl = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/10206.png",
                Moves = [],
                Types = ["normal"],
            };

            var transformData = new NamedApiResource()
            {
                Name = "transform",
                Url = "https://pokeapi.co/api/v2/move/144/"
            };

            var expectedDitto = new PokemonDto()
            {
                Id = 10206,
                Name = "ditto",
                SpriteUrl = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/132.png",
                Moves = [transformData],
                Types = ["normal"],
            };

            var snorlaxGmaxResult = await _pokemonController.GetDataForPokemonAsync(expectedSnorlaxGmax.Name);
            var dittoResult = await _pokemonController.GetDataForPokemonAsync(expectedDitto.Name);

            Assert.That(expectedSnorlaxGmax.Id, Is.EqualTo(snorlaxGmaxResult.Id));
            Assert.That(expectedSnorlaxGmax.Name, Is.EqualTo(snorlaxGmaxResult.Name));
            Assert.That(expectedSnorlaxGmax.SpriteUrl, Is.EqualTo(snorlaxGmaxResult.SpriteUrl));
            Assert.That(expectedSnorlaxGmax.Moves, Is.EqualTo(snorlaxGmaxResult.Moves));
            Assert.That(expectedSnorlaxGmax.Types, Is.EqualTo(snorlaxGmaxResult.Types));

            Assert.That(expectedDitto.Moves.First(), Is.EqualTo(dittoResult.Moves.First()));
        }

        [Test]
        public async Task GetAllPokemonNamesAsync_ShouldStoreData_IfNotFoundInLocalDbAsync()
        {
            await _pokemonControllerWithMockDeps.GetAllPokemonNamesAsync();

            _pokemonDataRepositoryMock.Verify(m => m.SavePokemonNames(It.IsAny<List<PokemonNameDto>>()), Times.Exactly(1));
        }

        [Test]
        public async Task GetDataForPokemonAsync_ShouldStoreData_IfNotFoundInLocalDbAsync()
        {
            await _pokemonControllerWithMockDeps.GetDataForPokemonAsync("ditto");

            _pokemonDataRepositoryMock.Verify(m => m.SavePokemonData(It.IsAny<PokemonDto>()), Times.Exactly(1));
        }
    }
}
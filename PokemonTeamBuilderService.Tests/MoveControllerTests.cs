﻿using PokemonTeamBuilderService.Controllers;
using PokemonTeamBuilderService.Models;
using Moq;
using PokemonTeamBuilderService.Repository;
using System.Data;

namespace PokemonTeamBuilderService.Tests
{
    [TestFixture]
    public class MoveControllerTests
    {
        private MoveController _moveController;
        private MoveController _moveControllerWithMockDeps;
        private Mock<IPokemonDataRepository> _pokemonDataRepositoryMock;

        [SetUp]
        public void InitController()
        {
            PokemonDataRepository dataRepository = new();

            this._moveController = new(dataRepository);

            _pokemonDataRepositoryMock = new Mock<IPokemonDataRepository>();
            _pokemonDataRepositoryMock.Setup(x => x.GetDataForMove(It.IsAny<string>()));
            _pokemonDataRepositoryMock.Setup(x => x.SaveMoveData(It.IsAny<MoveDto>()));

            this._moveControllerWithMockDeps = new(_pokemonDataRepositoryMock.Object);
        }

        [Test]
        public async Task GetMoveInformationAsync_ShouldReturnData_OnSuccessAsync()
        {
            var expectedDarkPulse = new MoveDto()
            {
                Id = 399,
                Name = "dark-pulse",
                Type = "dark",
                DamageClass = "special"
            };

            var darkPulseResult = await _moveController.GetMoveInformationAsync(expectedDarkPulse.Name);

            Assert.That(expectedDarkPulse.Id, Is.EqualTo(darkPulseResult.Id));
            Assert.That(expectedDarkPulse.Name, Is.EqualTo(darkPulseResult.Name));
            Assert.That(expectedDarkPulse.Type, Is.EqualTo(darkPulseResult.Type));
            Assert.That(expectedDarkPulse.DamageClass, Is.EqualTo(darkPulseResult.DamageClass));
        }

        [Test]
        public async Task GetMoveInformationAsync_ShouldStoreData_IfNotFoundInLocalDbAsync()
        {
            await _moveControllerWithMockDeps.GetMoveInformationAsync("tackle");

            _pokemonDataRepositoryMock.Verify(m => m.SaveMoveData(It.IsAny<MoveDto>()), Times.Exactly(1));
        }
    }
}

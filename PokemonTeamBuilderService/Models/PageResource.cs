﻿namespace PokemonTeamBuilderService.Models
{
    public class PageResource<T>
    {
        public T[] Results { get; set; } = [];
        public int Number { get; set; }
        public string? Next { get; set; }
        public string? Previous { get; set; }
    }
}

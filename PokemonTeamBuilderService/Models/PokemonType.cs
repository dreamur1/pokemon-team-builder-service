﻿namespace PokemonTeamBuilderService.Models
{
    public class PokemonType
    {
        public int Slot { get; set; }
        public required NamedApiResource Type { get; set; }
    }
}

﻿namespace PokemonTeamBuilderService.Models
{
    public class PokemonDto
    {
        public required int Id { get; set; }
        public required string Name { get; set; }
        public string? SpriteUrl { get; set; }
        public NamedApiResource[]? Moves { get; set; } = [];
        public required string[] Types { get; set; }
    }
}

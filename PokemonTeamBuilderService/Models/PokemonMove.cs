﻿namespace PokemonTeamBuilderService.Models
{
    // this is for the property that lives on the Pokemon object
    //  * DO NOT CONFUSE THIS FOR A "MOVERESPONSE" TYPE *
    public class PokemonMove
    {
        public required NamedApiResource Move { get; set; }
    }
}

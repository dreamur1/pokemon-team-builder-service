﻿using System.Text.Json.Serialization;

namespace PokemonTeamBuilderService.Models
{
    public class MoveDto
    {
        public required int Id { get; set; }
        public required string Name { get; set; }
        public required string Type { get; set; }
        public required string DamageClass { get; set; }
    }
}

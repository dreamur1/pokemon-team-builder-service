﻿namespace PokemonTeamBuilderService.Models
{
    public class PokemonNameDto
    {
        public int Id { get; set; }
        public required string Name { get; set; }
    }
}

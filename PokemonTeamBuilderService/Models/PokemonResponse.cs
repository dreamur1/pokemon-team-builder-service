﻿using System.Text.Json.Serialization;

namespace PokemonTeamBuilderService.Models
{
    public class PokemonResponse
    {
        public int Id { get; set; }
        public required string Name { get; set; } = "";

        public required PokemonSprite Sprites { get; set; }
        public PokemonMove[]? Moves { get; set; } = [];
        public required PokemonType[] Types { get; set; } = [];
    }
}

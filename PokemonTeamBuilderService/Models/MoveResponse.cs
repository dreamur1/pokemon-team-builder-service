﻿using System.Text.Json.Serialization;

namespace PokemonTeamBuilderService.Models
{
    public class MoveResponse
    {
        public required int Id { get; set; }
        public required string Name { get; set; }
        public required NamedApiResource Type { get; set; }
#pragma warning disable IDE1006 // Naming Styles
        public required NamedApiResource damage_class { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }
}

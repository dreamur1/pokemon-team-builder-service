using System.Data;
using System.Text;
using System.Text.Json;
using MySql.Data.MySqlClient;
using PokemonTeamBuilderService.Models;

namespace PokemonTeamBuilderService.Repository
{
    public class PokemonDataRepository : IPokemonDataRepository
    {
        private string ConnectionString { get; set; }
        private string dbName { get; set; }

        public PokemonDataRepository() {
            var serverName = Environment.GetEnvironmentVariable("DB_HOSTNAME") ?? "localhost"; 
            this.dbName = Environment.GetEnvironmentVariable("MYSQL_DATABASE") ??  "pokemon";
            var userName = Environment.GetEnvironmentVariable("MYSQL_USER") ?? "root";
            var password = Environment.GetEnvironmentVariable("MYSQL_PASSWORD") ??  "testPassword";
            var port = Environment.GetEnvironmentVariable("DB_PORT") ?? "8889";
            this.ConnectionString = string.Format("Server={0}; database={1}; UID={2}; password={3}; port={4}", serverName, dbName, userName, password, port);
            
            using var conn = new MySqlConnection(this.ConnectionString);
            conn.Open();
            using MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = $"CREATE TABLE IF NOT EXISTS {this.dbName}.Move (\r\n\tId INT UNSIGNED NOT NULL,\r\n\tName varchar(50) NOT NULL,\r\n\t`Type` varchar(10) NOT NULL,\r\n\tDamageClass varchar(10) NOT NULL,\r\n\tCONSTRAINT Move_PK PRIMARY KEY (Id)\r\n)\r\nENGINE=InnoDB\r\nDEFAULT CHARSET=utf8mb4\r\nCOLLATE=utf8mb4_0900_ai_ci;\r\n\r\nCREATE TABLE IF NOT EXISTS {this.dbName}.Pokemon (\r\n\tId INTEGER UNSIGNED NOT NULL,\r\n\tName varchar(100) NOT NULL,\r\n\tSpriteUrl varchar(150) NULL,\r\n\tMoves TEXT NULL,\r\n\tTypes varchar(20) NOT NULL,\r\n\tCONSTRAINT Pokemon_PK PRIMARY KEY (Id)\r\n)\r\nENGINE=InnoDB\r\nDEFAULT CHARSET=utf8mb4\r\nCOLLATE=utf8mb4_0900_ai_ci;\r\n\r\nCREATE TABLE IF NOT EXISTS {this.dbName}.PokemonName (\r\n\tId INT UNSIGNED NOT NULL,\r\n\tName varchar(100) NOT NULL,\r\n\tCONSTRAINT PokemonName_PK PRIMARY KEY (Id)\r\n)\r\nENGINE=InnoDB\r\nDEFAULT CHARSET=utf8mb4\r\nCOLLATE=utf8mb4_0900_ai_ci;\r\n";
            cmd.ExecuteNonQuery();
        }

        public List<PokemonNameDto> GetPokemonNames()
        {
            List<PokemonNameDto> storedNames = [];

            using (var conn = new MySqlConnection(this.ConnectionString))
            {
                conn.Open();
                using MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT * FROM PokemonName";
                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    PokemonNameDto nameEntry = new()
                    {
                        Id = reader.GetInt32("Id"),
                        Name = reader.GetString("Name"),
                    };
                    storedNames.Add(nameEntry);
                }
            }
            return storedNames;
        }

        public void SavePokemonNames(List<PokemonNameDto> names)
        {
            using var conn = new MySqlConnection(this.ConnectionString);
            conn.Open();
            using MySqlCommand cmd = conn.CreateCommand();

            var query = new StringBuilder();
            query.Append("INSERT INTO PokemonName\n(Id, Name)\nVALUES\n");
            names.ForEach((pokemon) =>
            {
                query.Append($"({pokemon.Id}, '{pokemon.Name}'),");
            });
            query.Remove(query.Length - 1, 1).Append(';');

            cmd.CommandText = query.ToString();
            cmd.ExecuteNonQuery();
        }

        public PokemonDto? GetDataForPokemon(string pokemonName)
        {
            PokemonDto? pokemonRecord = null;

            using (var conn = new MySqlConnection(this.ConnectionString))
            {
                conn.Open();
                using MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT * FROM Pokemon where Name = @pkmn_name";
                cmd.Parameters.AddWithValue("@pkmn_name", pokemonName);
                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    pokemonRecord = new()
                    {
                        Id = reader.GetInt32("Id"),
                        Name = reader.GetString("Name"),
                        SpriteUrl = reader.GetString("SpriteUrl"),
                        Moves = reader.IsDBNull("Moves") ? [] : JsonSerializer.Deserialize<NamedApiResource[]?>(reader.GetString("Moves")),
                        Types = reader.GetString("Types").Split(','),
                    };
                }
            }
            return pokemonRecord;
        }
   
        public void SavePokemonData(PokemonDto pokemon)
        {
            using var conn = new MySqlConnection(this.ConnectionString);
            conn.Open();
            using MySqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = $"INSERT INTO {this.dbName}.Pokemon\r\n(Id, Name, SpriteUrl, Moves, Types)\r\nVALUES(@pokeomon_id, @pokemon_name, @pokemon_spriteUrl, @pokemon_moves, @pokemon_types);";
            cmd.Parameters.AddWithValue("@pokeomon_id", pokemon.Id);
            cmd.Parameters.AddWithValue("@pokemon_name", pokemon.Name);
            cmd.Parameters.AddWithValue("@pokemon_spriteUrl", pokemon.SpriteUrl);
            cmd.Parameters.AddWithValue("@pokemon_moves", JsonSerializer.Serialize(pokemon.Moves));
            cmd.Parameters.AddWithValue("@pokemon_types", pokemon.Types.Length > 1 ? pokemon.Types[0] + ',' + pokemon.Types[1] : pokemon.Types[0]);
            cmd.ExecuteNonQuery();
        }

        public MoveDto? GetDataForMove(string moveName)
        {
            MoveDto? moveRecord = null;

            using (var conn = new MySqlConnection(this.ConnectionString))
            {
                conn.Open();
                using MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT * FROM Move where Name = @move_name";
                cmd.Parameters.AddWithValue("@move_name", moveName);
                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    moveRecord = new()
                    {
                        Id = reader.GetInt32("Id"),
                        Name = reader.GetString("Name"),
                        Type = reader.GetString("Type"),
                        DamageClass = reader.GetString("DamageClass"),
                    };
                }
            }
            return moveRecord;
        }

        public void SaveMoveData(MoveDto moveData)
        {
            using var conn = new MySqlConnection(this.ConnectionString);
            conn.Open();
            using MySqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = $"INSERT INTO {this.dbName}.Move\r\n(Id, Name, Type, DamageClass)\r\nVALUES(@move_id, @move_name, @move_type, @move_damageClass);";
            cmd.Parameters.AddWithValue("@move_id", moveData.Id);
            cmd.Parameters.AddWithValue("@move_name", moveData.Name);
            cmd.Parameters.AddWithValue("@move_type", moveData.Type);
            cmd.Parameters.AddWithValue("@move_damageClass", moveData.DamageClass);
            cmd.ExecuteNonQuery();
        }
    }
}

﻿using PokemonTeamBuilderService.Models;

namespace PokemonTeamBuilderService.Repository
{
    public interface IPokemonDataRepository
    {
        public List<PokemonNameDto> GetPokemonNames();
        public void SavePokemonNames(List<PokemonNameDto> names);
        public PokemonDto? GetDataForPokemon(string pokemonName);
        public void SavePokemonData(PokemonDto pokemon);
        public MoveDto? GetDataForMove(string moveName);
        public void SaveMoveData(MoveDto moveData);
    }
}

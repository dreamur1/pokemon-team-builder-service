﻿using Microsoft.AspNetCore.Mvc;
using PokemonTeamBuilderService.Models;
using PokemonTeamBuilderService.Repository;

namespace PokemonTeamBuilderService.Controllers
{
    [ApiController]
    public class MoveController(IPokemonDataRepository repo) : ControllerBase
    {
        private readonly HttpClient _httpClient = new();
        private readonly IPokemonDataRepository _dataRepository = repo;

        [HttpGet(template: "[controller]/{moveName}")]
        public async Task<MoveDto?> GetMoveInformationAsync(string moveName)
        {
            var savedMoveData = this._dataRepository.GetDataForMove(moveName);

            if (savedMoveData == null)
            {
                string dataRoute = $"https://pokeapi.co/api/v2/move/{moveName}";
                var response = await this._httpClient.GetFromJsonAsync<MoveResponse>(dataRoute);

                if (response == null)
                {
                    return null;
                }

                savedMoveData = new MoveDto()
                {
                    Id = response.Id,
                    Name = response.Name,
                    Type = response.Type.Name,
                    DamageClass = response.damage_class.Name
                };
                this._dataRepository.SaveMoveData(savedMoveData);
            }

            return savedMoveData;
        }
    }
}

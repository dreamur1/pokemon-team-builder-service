using Microsoft.AspNetCore.Mvc;
using PokemonTeamBuilderService.Models;
using PokemonTeamBuilderService.Repository;

namespace PokemonTeamBuilderService.Controllers
{
    [ApiController]
    public class PokemonController(IPokemonDataRepository repo) : ControllerBase
    {
        private readonly HttpClient _httpClient = new();
        private readonly IPokemonDataRepository _dataRepository = repo;

        [HttpGet(template: "[controller]/")]
        public async Task<IEnumerable<PokemonNameDto>> GetAllPokemonNamesAsync()
        {
            var savedNames = this._dataRepository.GetPokemonNames();

            if (savedNames.Count < 1)
            {
                string dataRoute = "https://pokeapi.co/api/v2/pokemon?limit=100";
                var pageResult = await this._httpClient.GetFromJsonAsync<PageResource<NamedApiResource>>(dataRoute);

                if (pageResult == null) { return []; }

                savedNames = pageResult.Results.Select((resource) => new PokemonNameDto { Id = int.Parse(resource.Url.Split('/')[6]), Name = resource.Name }).ToList();
                
                while (pageResult.Next != null) {
                    pageResult = await this._httpClient.GetFromJsonAsync<PageResource<NamedApiResource>>(pageResult.Next);
                    if (pageResult == null) {  break; }
                    var namesAsDtos = pageResult.Results
                                            .Select((resource, index) => new PokemonNameDto { Id = int.Parse(resource.Url.Split('/')[6]), Name = resource.Name })
                                            .Where(val => !val.Name.Contains("-mega") && !val.Name.Contains("-gmax"));                    
                    savedNames.AddRange(namesAsDtos.ToList());
                }

                this._dataRepository.SavePokemonNames(savedNames);               
            }

            return savedNames;
        }

        [HttpGet(template: "[controller]/{pokemonName}")]
        public async Task<PokemonDto?> GetDataForPokemonAsync(string pokemonName)
        {
            var savedPokemon = this._dataRepository.GetDataForPokemon(pokemonName);

            if (savedPokemon == null) {
                string dataRoute = $"https://pokeapi.co/api/v2/pokemon/{pokemonName}";
                var response = await this._httpClient.GetFromJsonAsync<PokemonResponse>(dataRoute);

                if (response == null)
                {
                    return null;
                }

                savedPokemon = new PokemonDto()
                {
                    Id = response.Id,
                    Name = response.Name,
                    SpriteUrl = response.Sprites.front_default,
                    Moves = response.Moves == null ? [] : response.Moves.Select(m => m.Move).ToArray(),
                    Types = response.Types.Select(t => t.Type.Name).ToArray(),
                };

                this._dataRepository.SavePokemonData(savedPokemon);
            }
            
            return savedPokemon;
        }
    }
}
